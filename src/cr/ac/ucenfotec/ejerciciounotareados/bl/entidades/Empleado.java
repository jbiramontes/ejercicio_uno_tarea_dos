

package cr.ac.ucenfotec.ejerciciounotareados.bl.entidades;

/**
 * @author Jorge Biramontes
 * @version 1.0
 */
public class Empleado {
    private String mnombre;
    private String mcedula;

    /**
     *
     * @return nombre del empleado
     */
    public String getMnombre() {
        return mnombre;
    }

    /**
     *
     * @param mnombre nombre del empleado
     */
    public void setMnombre(String mnombre) {
        this.mnombre = mnombre;
    }

    /**
     *
     * @return cedula del empleado
     */
    public String getMcedula() {
        return mcedula;
    }

    /**
     *
     * @param mcedula cedula del empleado
     */
    public void setMcedula(String mcedula) {
        this.mcedula = mcedula;
    }

    /**
     * constructor sin parametros
     */
    public Empleado() {
    }

    /**
     * Constructor completo
     * @param mnombre nombre del empleado
     * @param mcedula cedula del empleado
     */
    public Empleado(String mnombre, String mcedula) {
        this.mnombre = mnombre;
        this.mcedula = mcedula;
    }

    /**
     *
     * @return datos del empleado
     */
    @Override
    public String toString() {
        return "Empleado " +
                "nombre='" + mnombre + '\'' +
                ", cedula='" + mcedula + '\'' ;
    }
}
