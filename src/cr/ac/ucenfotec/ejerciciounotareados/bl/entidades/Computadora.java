
package cr.ac.ucenfotec.ejerciciounotareados.bl.entidades;

/**
 * @author Jorge Biramontes
 * @version 1.0
 */
public class Computadora {
    private String marca;
    private String modelo;
    private Empleado responsable;

    /**
     * @return marca de la computadora
     */
    public String getMarca() {
        return marca;
    }

    /**
     * @param marca marca de la computadora
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * @return modelo de la computadora
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * @param modelo modelo de la computadora
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    /**
     * @return empleado responsable del equipo computo
     */
    public Empleado getResponsable() {
        return responsable;
    }

    /**
     * @param responsable empleado responsable del equipo computo
     */
    public void setResponsable(Empleado responsable) {
        this.responsable = responsable;
    }

    /**
     * Constructor sin parametros
     */
    public Computadora() {

    }

    /**
     * Contructor con paramtros
     *
     * @param marca       marca de la computadora
     * @param modelo      modelo de la computadora
     * @param responsable responsable de la computadora
     */
    public Computadora(String marca, String modelo, Empleado responsable) {
        this.marca = marca;
        this.modelo = modelo;
        this.responsable = responsable;
    }

    @Override
    public String toString() {
        return "Computadora{" +
                "marca='" + marca + '\'' +
                ", modelo='" + modelo + '\'' +
                ", responsable=" + responsable +
                '}';
    }
}
