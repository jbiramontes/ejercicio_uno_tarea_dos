package cr.ac.ucenfotec.ejerciciounotareados.bl.logica;

import cr.ac.ucenfotec.ejerciciounotareados.bl.entidades.Computadora;
import cr.ac.ucenfotec.ejerciciounotareados.bl.entidades.Empleado;

import java.util.ArrayList;

/**
 * @author Jorge Biramontes
 * @version 1.0
 */
public class Gestor {

    private ArrayList<Empleado> arregloEmpleados = new ArrayList<>();
    public ArrayList<Computadora> arregloComputadoras = new ArrayList<>();


    /**
     * Guarda datos de empleado en el objeto
     *
     * @param nombre nombre empleado
     * @param cedula cedula empleado
     * @return objeto de Empleado
     */
    public Empleado agregarEmpleado(String nombre, String cedula) {
        Empleado otroEmpleado = new Empleado(nombre, cedula);
        return otroEmpleado;
    }

    /**
     * Agrega un objeto Computadora a un ArrayList
     *
     * @param marca        marca de la computadora
     * @param modelo       modelo de la computadora
     * @param otroEmpleado Datos de empleado responsable
     */
    public void agregarComputadora(String marca, String modelo, Empleado otroEmpleado) {
        Computadora nuevaPc = new Computadora(marca, modelo, otroEmpleado);
        arregloComputadoras.add(nuevaPc);

    }

    /**
     * Extrae los objetos de un arreglo
     *
     * @return un arreglo de objetos Computadoras
     */
    public ArrayList<Computadora> getArregloComputadoras() {
        return arregloComputadoras;
    }


}
