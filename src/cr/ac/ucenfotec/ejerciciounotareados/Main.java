package cr.ac.ucenfotec.ejerciciounotareados;

import cr.ac.ucenfotec.ejerciciounotareados.controlador.Controlador;

public class Main {

    public static void main(String[] args) {

        Controlador menuPrograma = new Controlador();

        menuPrograma.ejecutar();


    }
}
