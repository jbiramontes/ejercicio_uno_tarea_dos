
package cr.ac.ucenfotec.ejerciciounotareados.controlador;


import cr.ac.ucenfotec.ejerciciounotareados.bl.entidades.Computadora;
import cr.ac.ucenfotec.ejerciciounotareados.bl.logica.Gestor;
import cr.ac.ucenfotec.ejerciciounotareados.iu.IU;

import java.util.ArrayList;
/**
 * @author Jorge Biramontes
 * @version 1.0
 */

public class Controlador {


    private IU interfaz = new IU();
    private Gestor gestor = new Gestor();

    /***
     * Ingreso de opcion por parte del usuario
     * @return el entero ingresado por el usuario
     */
    public int opcionSeleccionada() {

        return interfaz.leerOpcion();

    }

    /**
     * Metodo que ejecuta programa
     */
    public void ejecutar() {
        int opcion = 0;

        do {
            interfaz.mensaje("Registre la computadora y el empleado responsable");
            String marca, modelo, nombre, cedula;
            interfaz.mensaje("Introduzca la marca de la computadora");
            marca = interfaz.leerDatos();
            interfaz.mensaje("Introduzca el modelo");
            modelo = interfaz.leerDatos();
            interfaz.mensaje("Introduzca nombre de empleado");
            nombre = interfaz.leerDatos();
            interfaz.mensaje("Introduzca la cedula del empleado");
            cedula = interfaz.leerDatos();
            //gestor.repetidos(otroEmpleado, gestor.arregloEmpleados, nombre);
            gestor.agregarComputadora(marca, modelo, gestor.agregarEmpleado(nombre, cedula));
            interfaz.mensaje("Registro exitoso");
            interfaz.mensaje("Registre el siguiente equipo si no desea registrar mas equipos digite 0");
            opcion = opcionSeleccionada();

        } while (opcion != 0);
        interfaz.mensaje("Lista de equipos registrados y su usuario respectivo");
        ListarComputadoras();
        interfaz.mensaje("Gracias por utilizar nuestro servicio");
    }

    /**
     * Extrae los datos de las computadoras registradas
     */
    private void ListarComputadoras() {
        ArrayList<Computadora> lista = gestor.getArregloComputadoras();
        for (Computadora e : lista) {
            interfaz.mensaje(e.toString());

        }
    }


}


